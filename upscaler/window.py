# window.py: main window
#
# Copyright (C) 2022 Upscaler Contributors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-only


from os.path import basename, splitext
import subprocess
import re
import time
from gi.repository import Adw, Gtk, GLib, Gdk, Gio, Pango, Gio
from sys import exit
from upscaler.threading import RunAsync
from upscaler.file_chooser import FileChooser

ALG_WARNINGS = [
    'vkQueueSubmit failed'
]

UPSCALE_FACTOR = 4

class AlgorithmFailed(Exception):
    """ Raised when the algorithm as failed. """
    def __init__(self, result_code, output):
        super().__init__()
        self.result_code = result_code
        self.output = output

    def __str__(self):
        return f'Algorithm failed.\nResult code: {self.result_code}\nOutput: {self.output}'

class AlgorithmWarning(Exception):
    """ Raised when the output could be damaged. """
    pass

@Gtk.Template(resource_path='/io/gitlab/theevilskeleton/Upscaler/gtk/window.ui')
class UpscalerWindow(Adw.ApplicationWindow):
    __gtype_name__ = 'UpscalerWindow'

    """ Declare child widgets. """
    toast = Gtk.Template.Child()
    stack_upscaler = Gtk.Template.Child()
    button_input = Gtk.Template.Child()
    action_image_size = Gtk.Template.Child()
    action_upscale_image_size = Gtk.Template.Child()
    button_upscale = Gtk.Template.Child()
    spinner_loading = Gtk.Template.Child()
    image = Gtk.Template.Child()
    # video = Gtk.Template.Child()
    combo_models = Gtk.Template.Child()
    string_models = Gtk.Template.Child()
    # spin_scale = Gtk.Template.Child()
    button_output = Gtk.Template.Child()
    label_output = Gtk.Template.Child()
    button_cancel = Gtk.Template.Child()
    progressbar = Gtk.Template.Child()
    drop_overlay = Gtk.Template.Child()

    """ Initialize function. """
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        """ Declare default models and variables. """
        self.model_images = {
            'realesrgan-x4plus': _('Photo'),
            'realesrgan-x4plus-anime': _('Cartoon/Anime'),
        }

        self.input_file_path = None
        content = Gdk.ContentFormats.new_for_gtype(Gio.File)
        self.target = Gtk.DropTarget(formats=content, actions=Gdk.DragAction.COPY)

        """ Display models. """
        for model in self.model_images.values():
            self.string_models.append(model)

        """ Connect signals. """
        self.button_input.connect('clicked', self.open_file)
        self.button_upscale.connect('clicked', self.__upscale)
        self.button_output.connect('clicked', self.__output_location)
        self.combo_models.connect('notify::selected', self.__set_model)
        self.button_cancel.connect('clicked', self.__cancel)
        self.target.connect('drop', self.__on_drop)
        self.target.connect('enter', self.__on_enter)
        self.target.connect('leave', self.__on_leave)
        self.add_controller(self.target)

        # self.spin_scale.connect('value-changed', self.__update_post_upscale_image_size)

        # self.model_videos = [
        #     'realesr-animevideov3',
        # ]

    def __on_file_open(self, input_file_path, pixbuf):
        """ Set variables. """
        self.input_file_path = input_file_path
        self.image_size = (pixbuf.get_width(), pixbuf.get_height())

        """ Display image. """
        self.action_image_size.set_subtitle(f'{self.image_size[0]} × {self.image_size[1]}')
        self.action_upscale_image_size.set_subtitle(f'{self.image_size[0] * UPSCALE_FACTOR} × {self.image_size[1] * UPSCALE_FACTOR}')
        self.image.set_pixbuf(pixbuf)

        """ Reset widgets. """
        self.label_output.set_label(_('(None)'))
        self.button_upscale.set_sensitive(False)
        self.button_upscale.set_has_tooltip(True)
        self.combo_models.set_selected(0)
        self.stack_upscaler.set_visible_child_name('stack_upscale')
        self.spinner_loading.stop()

    def __on_file_open_error(self, error):
        if error:
            self.stack_upscaler.set_visible_child_name('stack_invalid_image')

    """ Open file and display it. """
    def load_file(self, file_path):
        self.__load_gfile(Gio.File.new_for_path(file_path))

    """ Open gfile and display it. """
    def __load_gfile(self, file):
        FileChooser.load_file(file,
                              self.__on_file_open,
                              self.__on_file_open_error)

    """ Open a file chooser and load the file. """
    def open_file(self, *args):
        FileChooser.open_file(self,
                              self.input_file_path,
                              self.__on_file_open,
                              self.__on_file_open_error)

    """ Select output file location. """
    def __output_location(self, *args):
        def good(output_file_path):
            """ Set variables. """
            self.output_file_path = output_file_path

            """ Update widgets. """
            self.button_upscale.set_sensitive(True)
            self.button_upscale.set_has_tooltip(False)

            """ Trim long base name if necessary. """
            self.label_output.set_label(basename(self.output_file_path))
            self.label_output.set_ellipsize(Pango.EllipsizeMode.MIDDLE)

        def bad(message):
            if message:
                self.toast.add_toast(Adw.Toast.new(message))

        base_path = basename(splitext(self.input_file_path)[0])
        image_size = [x * UPSCALE_FACTOR for x in self.image_size]
        FileChooser.output_file(self,
                                f'{base_path}-{image_size[0]}x{image_size[1]}-upscaled.png',
                                good,
                                bad)

    def __on_drop(self, _, file, *args):
        self.__load_gfile(file)

    def __on_enter(self, *args):
        self.previous_stack = self.stack_upscaler.get_visible_child_name()
        self.stack_upscaler.set_visible_child_name('stack_drop')
        return Gdk.DragAction.COPY

    def __on_leave(self, *args):
        self.stack_upscaler.set_visible_child_name(self.previous_stack)

    """ Update progress. """
    def __upscale_progress(self, progress):
        if self.stack_upscaler.get_visible_child_name() == 'stack_upscaling':
            self.set_progress(progress)

    def __upscale(self, *args):

        """ Since GTK is not thread safe, prepare some data in the main thread. """
        self.cancelled = False

        """ Appropriately close child windows. """
        def reset_widgets():
            self.button_upscale.set_sensitive(True)
            self.progressbar.set_text(_('Loading…'))
            self.progressbar.set_fraction(0)
            self.cancelled = False

        """ Run in a separate thread. """
        def run():
            command = ['realesrgan-ncnn-vulkan',
                       '-i', self.input_file_path,
                       '-o', self.output_file_path,
                       '-n', list(self.model_images)[self.combo_models.get_selected()],
                       '-s', '4',
                       ]
            self.process = subprocess.Popen(command, stderr=subprocess.PIPE, universal_newlines=True)
            print('Running: ', end='')
            print(*command)
            """ Read each line, query the percentage and update the progress bar. """
            output = ""
            bad = False
            for line in iter(self.process.stderr.readline, ''):
                print(line, end='')
                output += line
                res = re.match('^(\d*.\d+)%$', line)
                if res:
                    GLib.idle_add(self.__upscale_progress, float(res.group(1)))
                else:
                    """ Check if this line is a warning. """
                    if bad: continue
                    for warn in ALG_WARNINGS:
                        if re.match(warn, line) is not None:
                            bad = True
                            continue
            """ Process algorithm output. """
            result = self.process.poll()
            if result != 0:
                raise AlgorithmFailed(result, output)
            if bad:
                raise AlgorithmWarning

        """ Run after run() function finishes. """
        def callback(result, error):
            if self.cancelled == True:
                self.toast.add_toast(Adw.Toast.new(_('Upscaling Cancelled')))
            else:
                self.upscaling_completed_dialog(error)

            self.stack_upscaler.set_visible_child_name('stack_upscale')
            reset_widgets()

        """ Run functions asynchronously. """
        RunAsync(run, callback)
        self.stack_upscaler.set_visible_child_name('stack_upscaling')
        self.button_upscale.set_sensitive(False)

    """ Ask the user if they want to open the file. """
    def upscaling_completed_dialog(self, error):
        def response(_widget):
            file = open(self.output_file_path, 'r')
            fid = file.fileno()
            connection = Gio.bus_get_sync(Gio.BusType.SESSION, None)
            proxy = Gio.DBusProxy.new_sync(connection,
                                           Gio.DBusProxyFlags.NONE,
                                           None,
                                           'org.freedesktop.portal.Desktop',
                                           '/org/freedesktop/portal/desktop',
                                           'org.freedesktop.portal.OpenURI',
                                           None)

            try:
                proxy.call_with_unix_fd_list_sync('OpenFile',
                                                  GLib.Variant('(sha{sv})', ('', 0, {'ask': GLib.Variant('b', True)})),
                                                  Gio.DBusCallFlags.NONE,
                                                  -1,
                                                  Gio.UnixFDList.new_from_array([fid]),
                                                  None)
            except Exception as e:
                print(f'Error: {e}')

        toast = None
        if error is None:
            toast = Adw.Toast.new(_('Image upscaled'))
            toast.set_button_label(_('Open'))
            toast.connect('button-clicked', response)
            self.toast.add_toast(toast)
        elif isinstance(error, AlgorithmWarning):
            toast = Adw.Toast.new(_('Image upscaled with warnings'))
            toast.set_button_label(_('Open'))
            toast.connect('button-clicked', response)
            self.toast.add_toast(toast)
        else:
            dialog = Adw.MessageDialog.new(self,
                                           _('Error while processing'),
                                           None)
            sw = Gtk.ScrolledWindow()
            sw.set_min_content_height(200)
            sw.set_min_content_width(400)
            sw.add_css_class('card')

            text = Gtk.Label()
            text.set_label(str(error))
            text.set_margin_top(12)
            text.set_margin_bottom(12)
            text.set_margin_start(12)
            text.set_margin_end(12)
            text.set_xalign(0)
            text.set_yalign(0)
            text.add_css_class('monospace')
            text.set_wrap(True)
            text.set_wrap_mode(Pango.WrapMode.WORD_CHAR)

            sw.set_child(text)
            dialog.set_extra_child(sw)

            def error_response(dialog, response_id):
                if response_id == 'copy':
                    clipboard = Gdk.Display.get_default().get_clipboard()
                    clipboard.set(str(error))
                    toast = Adw.Toast.new(_('Error copied to clipboard'))
                    self.toast.add_toast(toast)
                dialog.close()

            dialog.add_response('copy', _('_Copy to clipboard'))
            dialog.set_response_appearance('copy', Adw.ResponseAppearance.SUGGESTED)
            dialog.add_response('ok', _('_Dismiss'))
            dialog.connect('response', error_response)
            dialog.present()

    """ Set model and print. """
    def __set_model(self, *args):
        print(_('Model name: {}').format(list(self.model_images)[self.combo_models.get_selected()]))

    """ Update post-upscale image size as the user adjusts the spinner. """
    # def __update_post_upscale_image_size(self, *args):
    #     upscale_image_size = [
    #         self.image_size[1] * int(self.spin_scale.get_value()),
    #         self.image_size[2] * int(self.spin_scale.get_value()),
    #     ]
    #     self.action_upscale_image_size.set_subtitle(f'{upscale_image_size[0]} × {upscale_image_size[1]}')

    """ Update progress. """
    def set_progress(self, progress):
        self.progressbar.set_text(str(progress) + " %")
        self.progressbar.set_fraction(progress / 100)

    """ Prompt the user to close the dialog. """
    def close_dialog(self, function):
        self.stop_upscaling_dialog = Adw.MessageDialog.new(
            self,
            _('Stop upscaling?'),
            _('You will lose all progress.'),
        )
        def response(dialog, response_id):
            if response_id == 'stop':
                function()

        self.stop_upscaling_dialog.add_response('cancel', _('_Cancel'))
        self.stop_upscaling_dialog.add_response('stop', _('_Stop'))
        self.stop_upscaling_dialog.set_response_appearance('stop', Adw.ResponseAppearance.DESTRUCTIVE)
        self.stop_upscaling_dialog.connect('response', response)
        self.stop_upscaling_dialog.present()

    def start_loading(self):
        self.stack_upscaler.set_visible_child_name('stack_loading')
        self.spinner_loading.start()

    """ Cancel dialog. """
    def __cancel(self, *args):
        def function():
            self.cancelled = True
            self.process.kill()
        self.close_dialog(function)

    """ Close dialog. """
    def do_close_request(self):
        if self.stack_upscaler.get_visible_child_name() == 'stack_upscaling':
            def function():
                exit()
            self.close_dialog(function)
            return True
